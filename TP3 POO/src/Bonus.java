/**
 * Classe représentant un Pouvoir de type Bonus dans le jeu.
 * Cette classe sert principalement à regrouper tous les pouvoirs de bonus.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public abstract class Bonus extends Pouvoir {
    /**
     * Constructeur de la classe Bonus.
     * @param nom Le nom du pouvoir
     */
    public Bonus(String nom) {
        super(nom);
    }
}
