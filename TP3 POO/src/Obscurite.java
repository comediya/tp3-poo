/**
 * Classe représentant un pouvoir offensif de type Obscurite.
 * L'Obscurite est un pouvoir qui diminue la position du personnage dans la course en fonction du type de personnage.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Obscurite extends Pouvoir_Offensif {
    /**
     * Constructeur de la classe Obscurite.
     */
    public Obscurite() {
        super("Obscurite");
    }

    /**
     * Implémentation de la méthode appliquerPouvoir pour l'Obscurite.
     * Diminue la position du personnage dans la course en fonction du type de personnage.
     * @param personnage Le personnage sur lequel le pouvoir est appliqué
     */
    @Override
    public void appliquerEffet(Personnage personnage) {
        int positionActuelle = personnage.getPosition();
        if (personnage instanceof Nain) {
            personnage.setPosition(positionActuelle - 3);
        } else if (personnage instanceof Humain) {
            personnage.setPosition(positionActuelle - 4);
        } else if (personnage instanceof Elfe) {
            personnage.setPosition(positionActuelle - 5);
        }
    }
}
