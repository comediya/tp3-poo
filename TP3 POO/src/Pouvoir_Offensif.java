/**
 * Classe représentant un Pouvoir offensif dans le jeu.
 * Cette classe fournit une base pour les différents types de pouvoirs offensifs.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public abstract class Pouvoir_Offensif extends Pouvoir {
    /**
     * Constructeur de la classe PouvoirOffensif.
     * @param nom Le nom du pouvoir offensif
     */
    public Pouvoir_Offensif(String nom) {
        super(nom);
    }



}
