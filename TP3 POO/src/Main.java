/**
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Main {
    public static void main(String[] args) {

        Menu menu = new Menu();
        menu.afficherMenuPrincipal();
    }
}