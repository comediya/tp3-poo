import java.util.ArrayList;
import java.util.Scanner;

/**
 * Classe représentant le menu du jeu.
 */
public class Menu {
    private Scanner scanner;

    ArrayList<Personnage> personnages = new ArrayList<>();

    /**
     * Constructeur de la classe Menu.
     */
    public Menu() {
        this.scanner = new Scanner(System.in);
    }

    /**
     * Affiche le menu principal et récupère les choix de l'utilisateur.
     */
    public void afficherMenuPrincipal() {

        boolean sortie = true;

            int distance = demanderDistance();

            int nbPersonnages = demanderNbPersonnages();

            // Initialise une nouvelle course
            Course course = new Course(distance,personnages);

            for (int i = 0; i < nbPersonnages; i++) {
                String nom = demanderNom();
                int race = demanderRace();
                switch (race) {
                    case 1:
                        course.ajouterPersonnage(new Humain(nom));
                        break;
                    case 2:
                        course.ajouterPersonnage(new Elfe(nom));
                        break;
                    case 3:
                        course.ajouterPersonnage(new Nain(nom));
                        break;
                }
            }
            afficherParticipants();
            course.demarrerCourse();
        }



    private int demanderDistance() {
        System.out.println("Inscrivez la distance du trajet de la course:");
        return scanner.nextInt();
    }

    private int demanderNbPersonnages() {
        System.out.println("Combien de personnages participeront à la course:");
        return scanner.nextInt();
    }

    private String demanderNom() {
        System.out.println("Entrez le nom du personnage:");
        return scanner.next();
    }

    private int demanderRace() {
        System.out.println("Entrez la race du personnage:");
        System.out.println("1 - Humain: Aucun avantages ni inconvenients");
        System.out.println("2 - Elfe: Plus rapide, moins endurant");
        System.out.println("3 - Nain: Plus endurant, moins rapide");
        System.out.println("Choix:");
        return scanner.nextInt();
    }



    public void afficherParticipants() {
        System.out.println("Participants de la course :");
        for (Personnage personnage : personnages) {
            System.out.println("- " + personnage.getNom() +
                    "\n\t Position: " + personnage.getPosition() +
                    "\n\t Vitesse: " + personnage.getVitesse() +
                    "\n\t Résistance: " + personnage.getResistance());
            System.out.println("\t Pouvoirs : ");
            for (Pouvoir pouvoir : personnage.getPouvoirs()) {
                System.out.println("\t\t - " + pouvoir.getNom());
            }
        }
    }

}
