/**
 * Classe représentant un Elfe dans la course.
 *    @author Côme Diya Oumarou
 *    @version 1.0, Avril 2023
 */
public class Elfe extends Personnage {
    /**
     * Constructeur de la classe Elfe.
     * @param nom Le nom du personnage
     */
    public Elfe(String nom) {
        super(nom, 6, 1);
    }


    /**
     *la méthode utiliserPouvoir de la classe parente.
     * @param pouvoir Le pouvoir à utiliser
     */
    @Override
    public void utiliserPouvoir(Pouvoir pouvoir) {
        if (pouvoir instanceof Bonus) {
            Bonus bonus = (Bonus) pouvoir;
            bonus.appliquerEffet(this);
        } else if (pouvoir instanceof Pouvoir_Offensif) {
            // Un personnage ne peut pas utiliser un pouvoir offensif sur lui-même
            throw new IllegalArgumentException("Un Elfe ne peut pas utiliser un pouvoir offensif sur lui-même");
        }
    }
}
