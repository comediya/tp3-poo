import java.util.Random;

/**
 * Classe représentant un pouvoir de type Teleportation.
 * La Teleportation est un pouvoir de bonus qui augmente la position du personnage dans
 * la course d'un nombre entre 1 et 4.
 *
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Teleportation extends Bonus {
  /**
   *  Attribut pour la distance de la téléportation
   */
    private int distanceTeleportation;

    /**
     * Constructeur de la classe Teleportation.
     * Il initialise la distance de la téléportation à un nombre aléatoire entre 1 et 4.
     */
    public Teleportation() {
        super("Teleportation");
        Random rand = new Random();
        this.distanceTeleportation = rand.nextInt(4) + 1;
    }

    /**
     * Elle augmente la position du personnage dans la course de la distance de la téléportation.
     * @param personnage Le personnage sur lequel le pouvoir est appliqué
     */
    @Override
    public void appliquerEffet(Personnage personnage) {
        int positionActuelle = personnage.getPosition();
        personnage.setPosition(positionActuelle + this.distanceTeleportation);
    }
}
