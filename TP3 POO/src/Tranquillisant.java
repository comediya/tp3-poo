/**
 * Classe représentant un pouvoir offensif de type Tranquillisant.
 * Le Tranquillisant est un pouvoir qui diminue la vitesse du personnage de 1.
 *
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Tranquillisant extends Pouvoir_Offensif {
    /**
     * Constructeur de la classe Tranquillisant.
     */
    public Tranquillisant() {
        super("Tranquillisant");
    }

    /**
     * Diminue la vitesse du personnage de 1.
     * @param personnage Le personnage sur lequel le pouvoir est appliqué
     */
    @Override
    public void appliquerEffet(Personnage personnage) {
        int vitesseActuelle = personnage.getVitesse();
        personnage.setVitesse(vitesseActuelle - 1);
    }
}
