

import java.util.ArrayList;
/**
 * Classe représentant un personnage de la course.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public abstract class Personnage {

    /**
     * nom du personnage
     */
    private String nom ;

/*+
* vitesse du personnage
 */
    private int vitesse;

    /**
     * resistanse du personnage
     */
    private int resistance;

    /**
     * liste des pouvoirs du personnage
     */
    private ArrayList<Pouvoir> pouvoirs;

    /**
     * position du personnage dans la course
     */
    private int position;


    /**
     * Constructeur de la classe Personnage.
     * @param nom Le nom du personnage
     * @param vitesse La vitesse initiale du personnage
     * @param resistance La résistance initiale du personnage
     */
    public Personnage(String nom, int vitesse, int resistance) {
        this.nom = nom;
        this.vitesse = vitesse;
        this.position = 0;
        this.resistance = resistance;
    }



    public int getVitesse() {
        return vitesse;
    }

    public void setVitesse(int vitesse) {
        this.vitesse = vitesse;
    }

    public int getResistance() {
        return resistance;
    }

    public void setResistance(int resistance) {
        this.resistance = resistance;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }



    public ArrayList<Pouvoir> getPouvoirs() {
        return pouvoirs;
    }

    public void setPouvoirs(ArrayList<Pouvoir> pouvoirs) {
        this.pouvoirs = pouvoirs;
    }


    /**
     * Ajoute un pouvoir à la liste des pouvoirs du personnage.
     * @param pouvoir Le pouvoir à ajouter
     */
    public void ajouterPouvoir(Pouvoir pouvoir) {
        pouvoirs.add(pouvoir);
    }

    /**
     * Utilise un pouvoir.
     * @param pouvoir Le pouvoir à utiliser
     */
    public abstract void utiliserPouvoir(Pouvoir pouvoir);

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}


