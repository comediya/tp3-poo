/**
 * Classe représentant un pouvoir de type Stimulant.
 * Le Stimulant est un pouvoir de bonus qui augmente la vitesse du personnage de 1.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Stimulant extends Bonus {
    /**
     * Constructeur de la classe Stimulant.
     */
    public Stimulant() {
        super("Stimulant");
    }

    /**
     * Augmente la vitesse du personnage de 1.
     * @param personnage Le personnage sur lequel le pouvoir est appliqué
     */
    @Override
    public void appliquerEffet(Personnage personnage) {
        int vitesseActuelle = personnage.getVitesse();
        personnage.setVitesse(vitesseActuelle + 1);

    }
}
