import java.util.Random;

/**
 * Classe représentant un pouvoir offensif de type Obstacle.
 * L'Obstacle est un pouvoir qui diminue la position du personnage dans la course d'un nombre entre 1 et 4.
 *
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Obstacle extends Pouvoir_Offensif {
    /**
     * Attribut pour la distance de l'obstacle
     */
    private int distanceObstacle;

    /**
     * Constructeur de la classe Obstacle.
     * Elle initialise la distance de l'obstacle à un nombre aléatoire entre 1 et 4.
     */
    public Obstacle() {
        super("Obstacle");
        Random rand = new Random();
        this.distanceObstacle = rand.nextInt(4) + 1;
    }

    /**
     * Diminue la position du personnage dans la course de la distance de l'obstacle.
     * @param personnage Le personnage sur lequel le pouvoir est appliqué
     */
    @Override
    public void appliquerEffet(Personnage personnage) {
        int positionActuelle = personnage.getPosition();
        personnage.setPosition(positionActuelle - this.distanceObstacle);
    }
}
