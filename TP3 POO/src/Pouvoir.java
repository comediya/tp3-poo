/**
 * Classe abstraite représentant un Pouvoir générique dans le jeu.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public abstract class Pouvoir {
    /**
     * Nom du pouvoir
     */
    private String nom;
    /**
     * Constructeur de la classe Pouvoir.
     * @param nom Le nom du pouvoir
     */
    public Pouvoir(String nom) {
        this.nom = nom;
    }
    /**
     * Méthode pour obtenir le nom du pouvoir.
     * @return Le nom du pouvoir
     */
    public String getNom() {
        return nom;
    }
    /**
     * Méthode qui détermine comment le pouvoir affecte un personnage.
     * Doit être implémentée par chaque sous-classe de Pouvoir.
     * @param personnage Le personnage sur lequel le pouvoir est appliqué
     */
    public abstract void appliquerEffet(Personnage personnage);

}
