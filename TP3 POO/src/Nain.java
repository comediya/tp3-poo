/**
 * Classe représentant un Nain dans la course.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Nain extends Personnage {
    /**
     * Constructeur de la classe Nain.
     * @param nom Le nom du personnage
     */
    public Nain(String nom) {
        super(nom, 4, 3);  // Les Nains ont une vitesse de 4 et une résistance de 3
    }

    /**
     * La méthode utiliserPouvoir de la classe parente.
     * @param pouvoir Le pouvoir à utiliser
     */
    @Override
    public void utiliserPouvoir(Pouvoir pouvoir) {

        if (pouvoir instanceof Bonus) {
            Bonus bonus = (Bonus) pouvoir;
            bonus.appliquerEffet(this);
        } else if (pouvoir instanceof Pouvoir_Offensif) {
            // Un personnage ne peut pas utiliser un pouvoir offensif sur lui-même
            throw new IllegalArgumentException("Un Nain ne peut pas utiliser un pouvoir offensif sur lui-même");
        }
    }
}
