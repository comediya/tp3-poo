/**
 * Classe représentant un Humain dans la course.
 *
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Humain extends Personnage {
    /**
     * Constructeur de la classe Humain.
     * @param nom Le nom du personnage
     */
    public Humain(String nom) {
        super(nom, 5, 2);  // Les Humains ont une vitesse de 5 et une résistance de 2
    }

    /**
     * La méthode utiliserPouvoir de la classe parente.
     * @param pouvoir Le pouvoir à utiliser
     */
    @Override
    public void utiliserPouvoir(Pouvoir pouvoir) {
        if (pouvoir instanceof Bonus) {
            Bonus bonus = (Bonus) pouvoir;
            bonus.appliquerEffet(this);
        } else if (pouvoir instanceof Pouvoir_Offensif) {
            // Un personnage ne peut pas utiliser un pouvoir offensif sur lui-même
            throw new IllegalArgumentException("Un Humain ne peut pas utiliser un pouvoir offensif sur lui-même");
        }
    }
}
