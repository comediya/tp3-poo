/**
 * Classe représentant un pouvoir de type Chant.
 * Le Chant est un pouvoir de bonus qui augmente la position du personnage dans la course en fonction de son type.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Chant extends Bonus {
    /**
     * Constructeur de la classe Chant.
     */
    public Chant() {
        super("Chant");
    }


    /**
     * Augmente la position du personnage dans la course en fonction de son type.
     * @param personnage Le personnage sur lequel le pouvoir est appliqué
     */
    @Override
    public void appliquerEffet(Personnage personnage) {
        int positionActuelle = personnage.getPosition();

            if (personnage instanceof Nain) {
                personnage.setPosition(positionActuelle + 3);

            } else if (personnage instanceof Humain) {
                personnage.setPosition(positionActuelle + 4);

            } else if (personnage instanceof Elfe) {
                personnage.setPosition(positionActuelle + 5);

            }
    }
}
