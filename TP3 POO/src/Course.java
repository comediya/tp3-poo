import java.util.ArrayList;

/**
 * Classe représentant la course.
 * Contient les informations sur la course et gère le déroulement de la course.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Course {
    /**
     *  Attributs de la classe
      */

    private int distanceTotale;
    private ArrayList<Personnage> personnages;

    /**
     * Constructeur de la classe Course.
     * @param distanceTotale La distance totale de la course
     * @param personnages La liste des personnages qui participent à la course
     */
    public Course(int distanceTotale, ArrayList<Personnage> personnages) {
        this.distanceTotale = distanceTotale;
        this.personnages = personnages;
    }

    /**
     * Ajoute un personnage à la course.
     * @param personnage Le personnage à ajouter.
     */
    public void ajouterPersonnage(Personnage personnage) {
        personnages.add(personnage);
    }

    /**
     * Getter pour la distance totale de la course.
     * @return La distance totale de la course
     */
    public int getDistanceTotale() {
        return distanceTotale;
    }

    /**
     * Getter pour la liste des personnages.
     * @return La liste des personnages
     */
    public ArrayList<Personnage> getPersonnages() {
        return personnages;
    }

    /**
     * Méthode pour démarrer la course.
     */
    public void demarrerCourse() {

    }

    /**
     * Méthode pour terminer la course.
     */
    public void terminerCourse() {

    }
}
