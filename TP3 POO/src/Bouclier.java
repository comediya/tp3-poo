/**
 * Classe représentant un pouvoir de type Bouclier.
 * Le Bouclier est un pouvoir de bonus qui augmente la résistance du personnage de 1.
 * @author Côme Diya Oumarou
 * @version 1.0, Avril 2023
 */
public class Bouclier extends Bonus {
    /**
     * Constructeur de la classe Bouclier.
     */
    public Bouclier() {
        super("Bouclier");
    }


    /**
     * Augmente la résistance du personnage de 1.
     * @param personnage Le personnage sur lequel le pouvoir est appliqué
     */
    @Override
    public void appliquerEffet(Personnage personnage) {
        int resistanceActuelle = personnage.getResistance();
        personnage.setResistance(resistanceActuelle + 1);
    }
}
