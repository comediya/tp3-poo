# TP3 POO

# Jeu de course

Ce projet est un jeu de course simple en mode console où différents personnages (Humains, Elfes, Nains) avec différents pouvoirs (Bonus et Offensifs) se font la course.

## Créateur du projet

* Côme Diya Oumarou

## Licence

Ce projet est sous licence MIT.

## Description du projet

Dans ce jeu, chaque personnage a des caractéristiques de vitesse et de résistance et possède un ensemble de pouvoirs. Ces pouvoirs peuvent être utilisés pour augmenter leurs propres capacités ou diminuer les capacités des autres personnages. Le jeu continue jusqu'à ce qu'un personnage atteigne une certaine distance, à ce moment-là, le personnage ayant la plus grande distance parcourue est déclaré vainqueur.

## Guide de compilation et d'exécution

Pour compiler et exécuter le programme, assurez-vous d'abord que vous avez une version de Java compatible installée sur votre machine.

1. Ouvrez une fenêtre de terminal.
2. Naviguez jusqu'au répertoire du projet.
3. Pour compiler le projet, utilisez la commande : `javac *.java`.
4. Pour exécuter le projet, utilisez la commande : `java Main`.


# MIT License

Copyright (c) 2023 Côme Diya Oumarou

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.